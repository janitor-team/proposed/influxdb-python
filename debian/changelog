influxdb-python (5.3.1-3) UNRELEASED; urgency=medium

  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.5.1, no changes needed.

 -- Debian Janitor <janitor@jelmer.uk>  Wed, 25 Aug 2021 19:38:31 -0000

influxdb-python (5.3.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 16 Aug 2021 12:39:54 +0200

influxdb-python (5.3.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Thomas Goirand ]
  * New upstream release.
  * Team upload.
  * Add python3-msgpack as (build-)depends.
  * Refreshed skip-precision-test.patch.
  * Add remove-broken-test.patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 23 Mar 2021 09:54:46 +0100

influxdb-python (5.2.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
    Closes: #950063
  * Moved to Debian Python Modules Team
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 12 (routine-update)
  * Remove trailing whitespace in debian/copyright (routine-update)
  * Add salsa-ci file (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Tue, 10 Mar 2020 17:56:45 +0100

influxdb-python (5.2.0-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Removed Python 2 support (Closes: #935372, #936734).

 -- Thomas Goirand <zigo@debian.org>  Sat, 14 Sep 2019 21:35:49 +0200

influxdb-python (5.2.0-1) unstable; urgency=medium

  * New upstream version. (Closes: #899402)
  * Skip dataframe precision test. (Closes: #902470)
  * Remove unused patches.
  * d/control: depend on python3-six where
    appropriate. (Closes: #881662)
  * Fix insecure-copyright-format-uri.
  * Fix debian-watch-uses-insecure-uri.
  * Add autopkgtest.

 -- Alexandre Viau <aviau@debian.org>  Tue, 28 Aug 2018 12:44:31 -0400

influxdb-python (4.1.1-2) unstable; urgency=medium

  * Move to salsa.debian.org.

 -- Alexandre Viau <aviau@debian.org>  Fri, 29 Dec 2017 01:14:51 -0500

influxdb-python (4.1.1-1) unstable; urgency=medium

  * New upstream version (Closes: #868967)
  * Bump Standards-Version to 4.0.0.

 -- Alexandre Viau <aviau@debian.org>  Wed, 02 Aug 2017 16:11:46 -0400

influxdb-python (3.0.0-2) unstable; urgency=medium

  * Disable FurureWarnings (Closes: #841559)

 -- Alexandre Viau <aviau@debian.org>  Tue, 28 Feb 2017 15:44:10 -0500

influxdb-python (3.0.0-1) unstable; urgency=medium

  * New upstream version.
  * Bump Standards-Version to 3.9.8.
  * Change Vcs-Git and Vcs-Browser to secure (https) URLs.

 -- Alexandre Viau <aviau@debian.org>  Sun, 26 Jun 2016 21:53:39 +0200

influxdb-python (2.12.0-1) unstable; urgency=medium

  * New upstream version

 -- Alexandre Viau <aviau@debian.org>  Fri, 29 Jan 2016 10:10:50 -0500

influxdb-python (2.11.0-1) unstable; urgency=medium

  * New upstream version
  * Changed my email to @debian.org
  * Updated source link in d/copyright

 -- Alexandre Viau <aviau@debian.org>  Mon, 11 Jan 2016 19:48:22 -0500

influxdb-python (2.10.0-1) unstable; urgency=medium

  * New upstream version (Closes: #802079)

 -- Alexandre Viau <alexandre@alexandreviau.net>  Fri, 30 Oct 2015 16:14:53 -0400

influxdb-python (2.8.0-1) unstable; urgency=low

  * New upstream version
  * debian/copyright: only mention copyright holder
  * Changed Vcs-Browser link for cgit

 -- Alexandre Viau <alexandre@alexandreviau.net>  Tue, 18 Aug 2015 14:48:42 -0400

influxdb-python (2.7.3-1) unstable; urgency=medium

  * New upstream version
  * Re-enable tests
  * Re-order control fields

 -- Alexandre Viau <alexandre@alexandreviau.net>  Fri, 31 Jul 2015 10:20:24 -0400

influxdb-python (2.6.0-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Released new upstream version (Closes: #791879)

  [ Thomas Goirand ]
  * Fixed identical short and long description.

 -- Alexandre Viau <alexandre@alexandreviau.net>  Sat, 11 Jul 2015 19:21:32 -0400

influxdb-python (0.1.12-1) unstable; urgency=low

  * Initial release (Closes: #764643)

 -- Alexandre Viau <alexandre@alexandreviau.net>  Fri, 10 Oct 2014 12:56:09 -0400
